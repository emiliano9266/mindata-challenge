## Desafio Mindata - Backend JAVA


#### Construir la imagen de docker de la app con el siguiente comando:

    docker build . -t mindata-challenge

#### Correr la aplicacion con docker-compose con el siguiente comando

    docker-compose up -d --build

#### Para ver el logs del contenedor correr el siguiente comando

    docker logs -f mindata-challenge

#### Ver la documentacion de la API con Swagger

    Abrir la siguiente url en el navegador : http://localhost:7777/docs-ui.html


