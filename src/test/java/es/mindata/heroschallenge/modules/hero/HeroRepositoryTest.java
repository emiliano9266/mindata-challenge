package es.mindata.heroschallenge.modules.hero;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class HeroRepositoryTest {

    @Autowired
    HeroRepository heroRepository;

    @Test
    void whenFindHeroByName() {
        this.heroRepository.findByNameContainingIgnoreCase("man").forEach(hero -> {
            assertNotEquals(hero.getName().indexOf("man"), -1);
        });
    }

    @Test
    void whenFindHeroByNameWithEmptyName_shouldGetAllHeros() {
        assertEquals(this.heroRepository.findByNameContainingIgnoreCase("").size(), ((Collection<Hero>) this.heroRepository.findAll()).size());
    }
}