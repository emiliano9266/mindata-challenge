package es.mindata.heroschallenge.modules.hero;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.mindata.heroschallenge.modules.hero.dto.HeroConsume;
import es.mindata.heroschallenge.modules.hero.dto.HeroResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringJUnitConfig
@SpringBootTest(classes = {HeroService.class, ObjectMapper.class})
class HeroServiceTest {

    private String nameNewHero = "Poseidon";
    private String nameUpdateNewHero = "Hercules";

    @Autowired
    private HeroService heroService;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private HeroRepository heroRepository;

    @BeforeEach
    void configRepository() {
        Hero hero = new Hero();
        hero.setName(this.nameNewHero);
        hero.setId(100L);
        Mockito.when(heroRepository.save(Mockito.any(Hero.class))).thenReturn(hero);
        Mockito.when(heroRepository.findById(100L)).thenReturn(Optional.of(hero));
        Mockito.when(heroRepository.findById(200L)).thenReturn(Optional.empty());
    }

    @Test
    void whenCreateHero() {
        HeroConsume consume = new HeroConsume();
        consume.setName(this.nameNewHero);
        HeroResponse heroResponse = this.heroService.createHero(consume);
        assertNotNull(heroResponse, "Hero should be not null");
    }

    @Test
    void whenFindHeroById() {
        HeroResponse heroResponse = this.heroService.findById(100L);
        assertNotNull(heroResponse, "Hero should be not null");
        assertNotNull(heroResponse.getId(), "Hero id's should be not null");
    }

    @Test
    void whenNotFindHeroByIdThrowException() {
        assertThrows(ResourceNotFoundException.class, () -> {
            this.heroService.findById(200L);
        });
    }

    @Test
    void whenUpdateHero() {
        HeroConsume consume = new HeroConsume();
        consume.setName(this.nameUpdateNewHero);
        HeroResponse heroResponse = this.heroService.updateHero(100L, consume);
        assertNotNull(heroResponse, "Hero should be not null");
    }

    @Test
    void whenUpdateNonExistentHero() {
        HeroConsume consume = new HeroConsume();
        consume.setName(this.nameUpdateNewHero);

        assertThrows(ResourceNotFoundException.class, () -> {
            this.heroService.updateHero(200L, consume);
        });
    }
}