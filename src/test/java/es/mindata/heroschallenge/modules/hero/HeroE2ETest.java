package es.mindata.heroschallenge.modules.hero;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.mindata.heroschallenge.modules.hero.dto.HeroConsume;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.hasSize;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class HeroE2ETest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private HeroService heroService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @Order(0)
    public void createHeroWithOutName() throws Exception {
        HeroConsume heroConsume = new HeroConsume();
        heroConsume.setName("");

        this.mvc.perform(post("/hero")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(heroConsume)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(1)
    public void createHero() throws Exception {
        String body = "{\"name\":\"NewHero\"}";

        this.mvc.perform(post("/hero")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("NewHero")))
                .andExpect(jsonPath("$.id", notNullValue()));
    }

    @Test
    @Order(2)
    public void findById() throws Exception {

        this._findById(8L).andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", is("NewHero")))
                .andExpect(jsonPath("$.id", is(8)));
    }

    @Test
    @Order(3)
    public void findAll() throws Exception {

        this.mvc.perform(get("/hero")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(8)));
    }

    @Test
    @Order(4)
    public void updateHero() throws Exception {
        HeroConsume heroConsume = new HeroConsume();
        heroConsume.setName("UpdateNewHero");
        this.mvc.perform(put("/hero/8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(heroConsume)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("UpdateNewHero")))
                .andExpect(jsonPath("$.id", is(8)));
    }

    @Test
    @Order(5)
    public void findHeroByName() throws Exception {

        this.mvc.perform(get("/hero?name=Elon")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", containsString("Elon")));
    }

    @Test
    @Order(6)
    public void deleteHero() throws Exception {
        this.mvc.perform(delete("/hero/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        this._findById(1L).andExpect(status().isNotFound());
    }

    private ResultActions _findById(Long id) throws Exception {
        return this.mvc.perform(get("/hero/" + id)
                .contentType(MediaType.APPLICATION_JSON));
    }
}
