package es.mindata.heroschallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class HerosChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HerosChallengeApplication.class, args);
	}

}
