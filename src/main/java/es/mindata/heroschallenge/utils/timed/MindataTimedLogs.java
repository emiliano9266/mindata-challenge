package es.mindata.heroschallenge.utils.timed;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Component
@Aspect
public class MindataTimedLogs {

    private static Logger logger = LoggerFactory.getLogger(MindataTimedLogs.class);

    @Around("@annotation(es.mindata.heroschallenge.utils.timed.MindataTimed)")
    public Object timerMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Instant start = Instant.now();
        Object object = proceedingJoinPoint.proceed();
        Instant finish = Instant.now();
        logger.info("Method " + proceedingJoinPoint.getSignature().getDeclaringTypeName() + "|" + proceedingJoinPoint.getSignature().getName() + " finished in " + Duration.between(start, finish).toMillis() + "ms");
        return object;
    }

}
