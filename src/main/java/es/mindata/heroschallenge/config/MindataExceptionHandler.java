package es.mindata.heroschallenge.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.mindata.heroschallenge.utils.commondto.ResponseError;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Collectors;

@RestControllerAdvice
public class MindataExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ResponseError> handleResourceNotFoundException(final ResourceNotFoundException ex, final HttpServletRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<ResponseError>(new ResponseError(ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ResponseError> handleIllegalArgumentException(final MethodArgumentNotValidException ex, final HttpServletRequest request) {
        ex.printStackTrace();
        var responseError = new ResponseError();
        responseError.setMessages(ex.getAllErrors().stream().map((var objectError) -> {
            return "El campo " + (new ObjectMapper()).convertValue(ex.getAllErrors().get(0), Map.class).get("field") + " " + objectError.getDefaultMessage();
        }).collect(Collectors.toUnmodifiableList()));
        return new ResponseEntity<ResponseError>(responseError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ResponseError> handleGeneralException(final Exception ex, final HttpServletRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<ResponseError>(new ResponseError(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
