package es.mindata.heroschallenge.modules.hero.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class HeroConsume {
    @NotEmpty(message = "no debe estar vacio")
    @Size(min = 3, max = 30, message = "debe tener entre 3 y 30 caracteres")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
