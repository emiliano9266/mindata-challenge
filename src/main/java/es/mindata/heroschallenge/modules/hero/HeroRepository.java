package es.mindata.heroschallenge.modules.hero;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HeroRepository extends CrudRepository<Hero, Long> {
    List<Hero> findByNameContainingIgnoreCase(String name);
}
