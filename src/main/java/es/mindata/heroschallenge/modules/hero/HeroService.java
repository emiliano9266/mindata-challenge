package es.mindata.heroschallenge.modules.hero;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.mindata.heroschallenge.modules.hero.dto.HeroConsume;
import es.mindata.heroschallenge.modules.hero.dto.HeroResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HeroService {
    @Autowired
    private HeroRepository heroRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Caching(evict = {@CacheEvict(value = "heros", allEntries = true)})
    public HeroResponse createHero(HeroConsume consume) {
        Hero hero = this.objectMapper.convertValue(consume, Hero.class);
        return this.objectMapper.convertValue(
                this.heroRepository.save(hero),
                HeroResponse.class
        );
    }

    @Caching(evict = {
            @CacheEvict(value = "hero", allEntries = true),
            @CacheEvict(value = "heros", allEntries = true)})
    public HeroResponse updateHero(Long id, HeroConsume consume) {
        Hero hero = this.heroRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("El heroe que intenta actulizar no fue encontrado."));
        hero.setName(consume.getName());
        return this.objectMapper.convertValue(
                this.heroRepository.save(hero),
                HeroResponse.class
        );
    }

    @Cacheable("hero")
    public HeroResponse findById(Long id) {
        return this.objectMapper.convertValue(
                this.heroRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Heroe no encontrado.")),
                HeroResponse.class
        );
    }

    @Cacheable("heros")
    public List<HeroResponse> findByName(String name) {
        name = name.strip();
        return this.objectMapper.convertValue(
                this.heroRepository.findByNameContainingIgnoreCase(name),
                new TypeReference<List<HeroResponse>>() {
                }
        );
    }

    @Caching(evict = {
            @CacheEvict(value = "hero", allEntries = true),
            @CacheEvict(value = "heros", allEntries = true)})
    public void deleteHero(Long id) {
        Hero hero = this.heroRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Heroe no encontrado con ID " + id));
        this.heroRepository.delete(hero);
    }
}
