package es.mindata.heroschallenge.modules.hero;

import es.mindata.heroschallenge.modules.hero.dto.HeroConsume;
import es.mindata.heroschallenge.modules.hero.dto.HeroResponse;
import es.mindata.heroschallenge.utils.timed.MindataTimed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/hero")
public class HeroController {

    @Autowired
    private HeroService heroService;

    @MindataTimed
    @GetMapping("/{id}")
    @Operation(summary = "Obtener un heroe por su ID")
    public ResponseEntity<HeroResponse> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.heroService.findById(id));
    }

    @MindataTimed
    @GetMapping()
    @Operation(summary = "Obtener todos los heroes", description = "Se puede hacer uso del parametro name para filtrar por nombre, este parametro es optativo")
    public ResponseEntity<List<HeroResponse>> find(
            @Parameter(name = "name", example = "Superman")
            @RequestParam(name = "name", defaultValue = "", required = false) String name) {
        return ResponseEntity.ok(this.heroService.findByName(name));
    }

    @MindataTimed
    @PutMapping("/{id}")
    @Operation(summary = "Actualizar un heroe")
    public ResponseEntity<HeroResponse> updateHero(@PathVariable("id") Long id, @Valid @RequestBody HeroConsume heroConsume) {
        return ResponseEntity.ok(this.heroService.updateHero(id, heroConsume));
    }

    @MindataTimed
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Crear un heroe")
    public ResponseEntity<HeroResponse> createHero(@Valid @RequestBody HeroConsume heroConsume) {
        return new ResponseEntity<HeroResponse>(this.heroService.createHero(heroConsume), HttpStatus.CREATED);
    }

    @MindataTimed
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Eliminar un heroe por su ID")
    public ResponseEntity deleteHero(@PathVariable("id") Long id) {
        this.heroService.deleteHero(id);
        return new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
}
